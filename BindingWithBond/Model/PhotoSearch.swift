/**
 * Copyright (c) 2017 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import Foundation

// Provides an interface for querying the 500px search API
class PhotoSearch {
  
  let host = "api.imgur.com"
  let apiMethod = "/3/gallery/search"
  let clientId: String
  
  fileprivate static var formatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
    formatter.locale = Locale(identifier: "en_US_POSIX")
    return formatter
  }()
  
  init(clientId: String) {
    self.clientId = clientId
  }
  
  // Find photos that match the supplied query, results are returned asynchronously
  // via the supplied callback
  func findPhotos(_ query: PhotoQuery, callback: @escaping (Result<PhotoArray>) -> ())  {
    
    // convert the PhotoQuery into querystring parameters
    let params = [
      "q": query.text
    ];
    
    var urlComponents = URLComponents()
    urlComponents.scheme = "https"
    urlComponents.host = host
    urlComponents.path = apiMethod + query.sort + query.window + "/\(query.page)"
    urlComponents.queryItems = params.map { key, value in URLQueryItem(name: key, value: value) }
    
    // construct the query URL
    guard let url = urlComponents.url else {
      callback(.error(PhotoSearchError.malformedRequest))
      return
    }
    
    var request = URLRequest(url: url)
    request.httpMethod = "GET"
    request.setValue("Client-ID \(clientId)", forHTTPHeaderField: "Authorization")
    
    // perform the request
    let task = URLSession.shared.dataTask(with: request, completionHandler: {
      (data, response, error) in
        
        if let error = error {
            callback(Result.error(error))
        }
        else {
            // dispatch onto the main thread
            DispatchQueue.main.async {
                
                do {
                    // parse the results, then filter based on date
                    let result = try self.parseSearchResults(data!).filter { photo in
                        
                        if query.dateFilter {
                            return photo.date.timeIntervalSince(query.minDate as Date) > 0
                                && photo.date.timeIntervalSince(query.maxDate as Date) < 0
                        }
                        else {
                            return true
                        }
                    }
                    callback(Result.success(result))
                }
                catch {
                    callback(Result.error(PhotoSearchError.parseError))
                }
            }
        }
    })
    
    task.resume()
  }
  
  // parses the JSON data returned by the 50px API
  fileprivate func parseSearchResults(_ data: Data) throws -> PhotoArray {
    // convert the JSON response into a dictionary
    guard
        let jsonDict = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? NSDictionary,
        let posts = jsonDict["data"] as? [NSDictionary] else {
        throw PhotoSearchError.parseError
    }
    
    var images = [NSDictionary]()
    for post in posts {
        if let imageDictArray = post["images"] as? [NSDictionary] {
            for imageDict in imageDictArray {
                if let type = imageDict["type"] as? String {
                    if type == "image/jpeg" || type == "image/png" {
                        images.append(imageDict)
                    }
                }
            }
        }
    }
    
    let parsedPhotos = images.map {
          imageDict -> Photo? in
          // parse each photo instance - if an error occurs, return nil
          guard let imageUrl = imageDict["link"] as? String,
            let id = imageDict["id"] as? String,
            let dateString = imageDict["datetime"] as? Double,
            let date = PhotoSearch.formatter.date(from: PhotoSearch.formatter.string(from: Date(timeIntervalSince1970: dateString))),
            let url = URL(string: imageUrl) else { return nil }
        
          return Photo(id: id, url: url, date: date)
    }.compactMap { $0 } // flatMap to unwrap optionals and remove nils

    
    return parsedPhotos;
  }
  
}
