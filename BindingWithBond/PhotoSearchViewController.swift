/**
 * Copyright (c) 2017 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import UIKit

class PhotoSearchViewController: UIViewController {
    
    private let viewModel = PhotoSearchViewModel()
  
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var resultsTable: UITableView!
  
    override func viewDidLoad() {
        super.viewDidLoad()
    
        bindViewModel()
    }
    
    func bindViewModel() {
        viewModel.searchString.bidirectionalBind(to: searchTextField.reactive.text)
        viewModel.validSearchText
            .map { $0 ? .black : .red }
            .bind(to: searchTextField.reactive.textColor)
        
        // Table view binding
        viewModel.searchResults.bind(to: resultsTable) { dataSource, indexPath, tableView in
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath) as! PhotoTableViewCell
            let photo = dataSource[indexPath.row]
            cell.title.text = photo.id
            
            let backgroundQueue = DispatchQueue(label: "backgroundQueue", qos: .background, attributes: DispatchQueue.Attributes.concurrent, autoreleaseFrequency: DispatchQueue.AutoreleaseFrequency.inherit, target: nil)
            cell.photo.image = nil
            backgroundQueue.async {
                if let imageData = try? Data(contentsOf: photo.url) {
                    
                    DispatchQueue.main.async {
                        cell.photo.image = UIImage(data: imageData)
                    }
                    
                }
            }
            
            return cell
        }
        
        viewModel.searchInProgress
            .map { !$0 }.bind(to: activityIndicator.reactive.isHidden)
        viewModel.searchInProgress
            .map {$0 ? 0.5 : 1.0}.bind(to: resultsTable.reactive.alpha)
        
        _ = viewModel.errorMessages.observeNext { [unowned self] error in
            
            DispatchQueue.main.async {
                
                let alertController = UIAlertController(title: "Something went wrong :-(", message: error, preferredStyle: .alert)
                self.present(alertController, animated: true, completion: nil)
                let actionOk = UIAlertAction(title: "OK", style: .default,
                                             handler: { action in alertController.dismiss(animated: true, completion: nil) })
                
                alertController.addAction(actionOk)
                
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "ShowSettings" {
            
            let navVC = segue.destination as! UINavigationController
            let settingsVC = navVC.topViewController as! SettingsViewController
            settingsVC.viewModel = viewModel.searchMetadataViewModel
        }
        
    }

}
