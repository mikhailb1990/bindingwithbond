//
//  File.swift
//  BindingWithBond
//
//  Created by Mikhail Bhuta on 3/3/19.
//  Copyright © 2019 Mikhail Bhuta. All rights reserved.
//

import Foundation
import Bond

class PhotoSearchMetadataViewModel {
    let sort = Observable<String>("/hot")
    let window = Observable<String>("/day")
    let page = Observable<Int>(1)
    let dateFilter = Observable<Bool>(false)
    let minUploadDate = Observable<Date>(Date())
    let maxUploadDate = Observable<Date>(Date())
    
    init() {
        _ = maxUploadDate.observeNext {
            [unowned self]
            maxDate in
            if maxDate.timeIntervalSince(self.minUploadDate.value) < 0 {
                self.minUploadDate.value = maxDate
            }
        }
        _ = minUploadDate.observeNext {
            [unowned self]
            minDate in
            if minDate.timeIntervalSince(self.maxUploadDate.value) > 0 {
                self.maxUploadDate.value = minDate
            }
        }
    }
}
