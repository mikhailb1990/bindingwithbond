//
//  PhotoSearchViewModel.swift
//  BindingWithBond
//
//  Created by Mikhail Bhuta on 2/28/19.
//  Copyright © 2019 Mikhail Bhuta. All rights reserved.
//

import Foundation
import Bond
import ReactiveKit

class PhotoSearchViewModel {
    
    let searchMetadataViewModel = PhotoSearchMetadataViewModel()
    
    let searchString = Observable<String?>("")
    let validSearchText = Observable<Bool>(false)
    
    let searchResults = MutableObservableArray<Photo>([])
    
    let errorMessages = PublishSubject<String, NoError>()
    
    private let searchService: PhotoSearch = {
        let clientId = Bundle.main.object(forInfoDictionaryKey: "ImgurCID") as! String
        return PhotoSearch(clientId: clientId)
    }()
    
    let searchInProgress = Observable<Bool>(false)
    
    init() {
        
        _ = searchString
            .map { $0!.count >= 3 }
            .bind(to:validSearchText)
        
        _ = searchString
            .filter { $0!.count >= 3 }
            .throttle(seconds: 0.5)
            .observeNext { [unowned self] text in
                
                if let text = text {
                    self.executeSearch(text)
                }
        }
        
        _ = combineLatest(searchMetadataViewModel.dateFilter, searchMetadataViewModel.maxUploadDate,
                          searchMetadataViewModel.minUploadDate, searchMetadataViewModel.sort)
            .throttle(seconds: 0.5)
            .observeNext {
                [unowned self] _ in
                self.executeSearch(self.searchString.value!)
        }
        
    }
    
    func executeSearch(_ text: String) {
        var query = PhotoQuery()
        query.text = searchString.value ?? ""
        query.sort = searchMetadataViewModel.sort.value
        query.window = searchMetadataViewModel.window.value
        query.page = searchMetadataViewModel.page.value
        
        searchInProgress.value = true
        
        searchService.findPhotos(query) { (result) in
            
            self.searchInProgress.value = false
            switch result {
                case .success(let photos):
                    print("Imgur returned \(photos.count) photos")
                    self.searchResults.removeAll()
                    self.searchResults.insert(contentsOf: photos, at: 0)
                case .error:
                    self.errorMessages.next("There was an API request issue of some sort. Go ahead, hit me with that 1-star review!")
            }
            
        }
    }
}
