import UIKit
import ReactiveKit

let counter = Signal<Int, NoError> { observer in
    
    observer.next(1)
    observer.next(2)
    observer.next(3)
    
    observer.completed()
    
    return BlockDisposable {
        
    }
    
}

counter.observeNext { int in
    
    print(int)
    
}
